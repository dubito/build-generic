#!/usr/bin/env sh

SCRIPTPATH=$(readlink -f "$0")
THISDIR=$(dirname "$SCRIPTPATH")

TARGET=${1:-alpine}

TARGETSDIR=${THISDIR}/targets
DEFAULTDIR=${TARGETSDIR}/default
TARGETDIR=${TARGETSDIR}/${TARGET}
FILESDIR=${TARGETDIR}/files
UNITDIR=${THISDIR}/.units

NBDDEV=/dev/nbd0

source "$DEFAULTDIR/common.inc"


if [[ $(id -u) -ne 0 ]]; then
	echo "Please run this script with superuser privledges"
	exit
fi

add_ancestor () {
    ANCESTOR=$(eval "$(cat ${TARGETSDIR}/$1/config.inc)" && echo $INHERIT)
    if [ ! -z "${ANCESTOR}" ]; then
        echo "$(add_ancestor ${ANCESTOR}) ${ANCESTOR}"
    fi
}

inc () {
    if [[ -f "$DEFAULTDIR/${1}" ]]; then
        source "$DEFAULTDIR/${1}"
    else
        echo "necessary include file $1 not found in default target. Aborting..."
        exit
    fi

    for target in ${INHERITLIST} ${TARGET}; do
        if [[ -f "$TARGETSDIR/$target/${1}" ]]; then
            source "$TARGETSDIR/$target/${1}"
        fi
    done
}

run_phase () {
    if [ ! -z "$(eval "echo $(echo \$HOST_$1)")" ]; then
        eval "$(eval echo \"\$HOST_$1\")"
    fi
    if [ ! -z "$(eval "echo $(echo \$TARGET_$1)")" ]; then
        echo "$(eval echo \"\$TARGET_$1\")" | chroot ${IMGMOUNT} /bin/sh -l -
    fi
}

INHERITLIST=$(add_ancestor ${TARGET})

inc "config.inc"

IMGFILE=${2:-$IMGFILE_DEFAULT}
IMGSIZE=${IMGSIZE:-$IMGSIZE_DEFAULT}

HOSTNAME=${IMGHOSTNAME:-$(basename $IMGFILE | cut -f 1 -d '.') }

modprobe nbd
qemu-img create -f qcow2 ${IMGFILE} ${IMGSIZE}
qemu-nbd -c ${NBDDEV} ${IMGFILE}

inc "mkimage.inc"
partition ${NBDDEV} ||  error "Error partioning device!" 

inc "basesystem.inc"
ROOTFS_TAR=${ROOTFS_TAR:-$ROOTFS_TAR_DEFAULT}
KERNEL_TAR=${KERNEL_TAR:-$KERNEL_TAR_DEFAULT}
KERNEL_FILENAME=$(basename ${KERNEL_TAR})
KERNEL_VERSION="${filename%.tar.*}"

install_root ${ROOTFS_TAR} ${IMGMOUNT} || error "Error installing rootfs!"
install_kernel ${KERNEL_TAR} ${IMGMOUNT} || error "Error installing kernel!"

mount_system ${IMGMOUNT}

#overlay unit files of all inherited targets.
mkdir -p ${UNITDIR}
rm -f ${UNITDIR}/*
for target in default ${INHERITLIST} ${TARGET}; do
    for file in ${TARGETSDIR}/${target}/units/*.unit; do
        if [ -f "$file" ]; then cat "$file" >> ${UNITDIR}/$(basename $file); fi
    done
done

# run scripts in the chrooted environment
for file in ${UNITDIR}/*.unit; do
    unset HOST_PRE TARGET_PRE HOST_INSTALL TARGET_INSTALL HOST_POST TARGET_POST
    source $file
    for phase in PRE INSTALL POST; do run_phase $phase; done
done

# Teardown
cleanup
