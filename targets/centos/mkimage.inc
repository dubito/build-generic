EFIPARTSIZE_MB=148MiB

PARTITION_NAMES='efi boot system'
PARTITION_TYPES='fat32 ext4 ext4'
PARTITION_STARTS="0% ${EFIPARTSIZE_MB} 384MiB"
PARTITION_ENDS="${EFIPARTSIZE_MB} 384MiB 100%"

mount_partitions () {
    mkdir -p ${IMGMOUNT}
    mount ${1}p${SYSTEMNUM} ${IMGMOUNT}

    # mount boot and efi partions
    mkdir -p ${IMGMOUNT}/boot/
    mount ${1}p${BOOTNUM} ${IMGMOUNT}/boot/
    mkdir -p ${IMGMOUNT}/boot/efi/
    mount ${1}p1 ${IMGMOUNT}/boot/efi/
}

format_partitions () {
    mkfs.fat -F 32 ${1}p1
    mkfs.ext4 ${1}p${BOOTNUM} 
    mkfs.$(echo $PARTITION_TYPES | cut -d " " -f $((SYSTEMNUM))) ${1}p${SYSTEMNUM}
}
