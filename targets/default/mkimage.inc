EFIPARTSIZE_MB=148

PARTITION_NAMES='boot system'
PARTITION_TYPES='fat32 ext4'
PARTITION_STARTS="0% ${EFIPARTSIZE_MB}"
PARTITION_ENDS="${EFIPARTSIZE_MB} 100%"

mount_partitions () {
    mkdir -p ${IMGMOUNT}
    mount ${1}p${SYSTEMNUM} ${IMGMOUNT}

    # mount boot partion
    mkdir -p ${IMGMOUNT}/boot/
    mount ${1}p${BOOTNUM} ${IMGMOUNT}/boot/
}

format_partitions () {
    mkfs.fat -F 32 ${1}p${BOOTNUM}
    mkfs.$(echo $PARTITION_TYPES | cut -d " " -f $((SYSTEMNUM))) ${1}p${SYSTEMNUM}
}

partition () {
    CMD_PARTED="parted --script ${1} \n mklabel gpt \n"
    
    for i in `echo $PARTITION_NAMES | wc -w | xargs seq`;
    do
        PARTITION_NAME=$(echo $PARTITION_NAMES | cut -d " " -f $i)
        PARTITION_TYPE=$(echo $PARTITION_TYPES | cut -d " " -f $i)
        PARTITION_START=$(echo $PARTITION_STARTS | cut -d " " -f $i)
        PARTITION_END=$(echo $PARTITION_ENDS | cut -d " " -f $i)
        
        CMD_PARTED="$CMD_PARTED mkpart ${PARTITION_NAME} ${PARTITION_TYPE} ${PARTITION_START} ${PARTITION_END}\n"
        if [[ "${PARTITION_NAME}" == "boot" ]]; then BOOTNUM=$i; fi
        if [[ "${PARTITION_NAME}" == "system" ]]; then SYSTEMNUM=$i; fi
    done

    CMD_PARTED="$CMD_PARTED set $BOOTNUM boot on \nset $BOOTNUM esp on"
    eval `echo -e $CMD_PARTED`
    
    partx -a ${1}


    format_partitions "${1}"
    mount_partitions  "${1}"
}
