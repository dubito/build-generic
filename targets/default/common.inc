cleanup () {
	for dir in "${IMGMOUNT}/boot/efi" \
           "${IMGMOUNT}/boot" \
		   "${IMGMOUNT}/proc" \
		   "${IMGMOUNT}/sys" \
		   "${IMGMOUNT}/dev" \
		   "${IMGMOUNT}"
	do
		if mount | grep $dir > /dev/null; then
			umount $dir
		fi
	done
	qemu-nbd -d ${NBDDEV}

}

error () {
    echo "ERROR: $1"
    cleanup
    exit
}

mount_system () {
    #perpare for chroot
    mount --bind /dev ${1}/dev/
    mount --bind /sys ${1}/sys/
    mount -t proc none ${1}/proc/
    cp /etc/resolv.conf ${1}/etc/
}
