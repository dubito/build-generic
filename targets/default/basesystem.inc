ROOTFS_TAR_DEFAULT="rootfs.tar.gz"
KERNEL_TAR_DEFAULT=$(ls -t kernel*.tar* | head -1)

extract () {
    filename=$(basename "${1}")
    ext="${filename##*.}"

    if [[  "${ext}" == "tar" ]]; then
        echo "Extract tar"
        tar -C "${2}/" -xvpf "${1}"
    fi
    if [[ "${ext}" == "gz" ]]; then
        echo "Extract gz"
        tar -C "${2}/" -zxvpf "${1}"
    fi
    if [[ "${ext}" == "xz" ]]; then
        echo "Extract xz"
        xz -cd "${1}" | tar -C "${2}/" -xvp
    fi
}

install_root () {
    # Copy alpine rootfs
    extract "${1}" "${2}"
}


install_kernel () {
    # Copy the kernel+
    extract "${1}" "${2}"
}
