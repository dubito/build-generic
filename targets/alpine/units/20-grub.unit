ROOT_DEVICE=$(blkid ${NBDDEV}p2 -o value -s PARTUUID)
GRUB_DEVICE=$(blkid ${NBDDEV}p1 -o value -s UUID)

echo "GRUB device is ${GRUB_DEVICE}"
echo "root device is ${ROOT_DEVICE}"

TARGET_INSTALL=$(cat <<-END
    apk add grub-efi grub
END
)

HOST_POST=$(cat <<-END
    mkdir -p ${IMGMOUNT}/boot/grub
    cp -r ${IMGMOUNT}/usr/lib/grub/arm64-efi ${IMGMOUNT}/boot/grub/
    
    touch ${IMGMOUNT}/etc/update-extlinux.conf
    mkdir -p ${IMGMOUNT}/etc/default/
    
    cat <<-EOF > ${IMGMOUNT}/etc/default/grub
    GRUB_CMDLINE_LINUX_DEFAULT="console=ttyAMA0,115200 init=/sbin/init rootwait rw"
    GRUB_DEVICE_UUID=${ROOT_DEVICE}
    GRUB_TERMINAL="serial"
    GRUB_SERIAL_COMMAND="serial --speed=115200 --word=8 --parity=no --stop=1 efi0"
EOF

    # In a perfect world, grub-install could sort this, but it isn't working for me here
    cat <<-EOF > ${IMGMOUNT}/boot/grub/search.cfg
    search --set=root --fs-uuid ${GRUB_DEVICE} --hint hd0,gpt1
    prefix=(\\\$root)/grub
EOF
    mkdir -p ${IMGMOUNT}/boot/EFI/boot/
END
)

TARGET_POST=$(cat <<-END
    grub-mkconfig -o /boot/grub/grub.cfg
    # As grub doesn't want to generate with a UUID (even when specified, force it)
    sed -i "s/root=\/dev\/nbd0p2/root=PARTUUID=${ROOT_DEVICE}/g" /boot/grub/grub.cfg
    grep "PARTUUID=${ROOT_DEVICE}" /boot/grub/grub.cfg
    if [[ "$?" -ne 0 ]]; then
        echo "ERROR: PARTUUID is not in the generated grub.cfg, the script may need to be adjusted"
        echo "NOTE: ${IMGMOUNT} is still mounted for debugging purposes"
        exit
    fi
    
    grub-mkimage -c /boot/grub/search.cfg --format=arm64-efi -d /usr/lib/grub/arm64-efi --output=/boot/EFI/boot/bootaa64.efi -p "" \
    /usr/lib/grub/arm64-efi/part_gpt \
    /usr/lib/grub/arm64-efi/disk /usr/lib/grub/arm64-efi/search \
    /usr/lib/grub/arm64-efi/search_fs_uuid /usr/lib/grub/arm64-efi/fshelp \
    /usr/lib/grub/arm64-efi/search_label /usr/lib/grub/arm64-efi/fat
END
)
