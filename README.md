## build-generic
The build-generic is a toolset to build arm64 VM images that can be booted from uefi. It does so by extracting a suited kernel image / roots tar-ball and running a set of operations 
in the newly created system. The builder can be used for various distributions and configurations by creating respective 'targets'.

Different configurations of a certain distribution can be easily created by inheriting a generic target of the wanted distribution.

### Requirements
In order to run the generic build script in muvirt, the following packages need to be installed:

  * blkid
  * partx-utils
  * parted

for rockstor you will need additionally:

  * btrfs
  * xz

### Usage
The build script can be used to build a specific target and to directly write it into a file or a block device.

```shell
./build.sh target output
```

if no target is specified the alpine target is built as a default target.

### Customize build script behaviour
There are a number of environment variables that can be used to customize the build script behaviour. Those are:

| Variable      | Meaning                                                                |
|---------------|------------------------------------------------------------------------|
| ROOTFS_TAR    | path to a (compressed) tar archive containing the root file system     |
| KERNEL_TAR    | path to a (compressed) tar archive containing the kernel files         | 
| IMGSIZE       | size of the created image (modifiers k,M,G cam be used)                |
| IMGHOSTNAME   | hostname set inside the VM (default: autodetect from output file name) |

if those values are not specified by the user, default values are taken from the target that is built.
